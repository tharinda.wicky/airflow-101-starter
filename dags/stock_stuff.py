from datetime import datetime

from airflow.models import DAG, TaskInstance, Param
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.providers.mongo.hooks.mongo import MongoHook
from pymongo import MongoClient

dag = DAG(dag_id='stock_processing', schedule_interval=None, start_date=datetime(2022, 2, 10), catchup=False)


def build_model():
    print('Building model..')

def test_model():
    print('Testing model..')

dummy_task = DummyOperator(task_id='dummy', dag=dag)

screening_model_op = PythonOperator(task_id='screening_model', python_callable=build_model)

back_test_op = PythonOperator(task_id='back_test', python_callable=test_model)

dummy_task.set_downstream(screening_model_op)
screening_model_op.set_downstream(back_test_op)
