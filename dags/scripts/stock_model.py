import pandas as pd

from datetime import datetime, timedelta

from airflow import DAG
from airflow.models import TaskInstance, Param
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.providers.mongo.hooks.mongo import MongoHook
from pymongo import MongoClient


# monotonic transformation
def rank_factor(base_data, factor, ascending_order):
    # print('Ranking ' + factor + '...')
    base_data['rank_perc_' + factor] = base_data[factor].rank(ascending=ascending_order, na_option='top', pct=True)


def calculate_composite_score(base_data, q_date, quarter, base_data_array, base_data_ascending_order_array):
    top_stock = 25
    base_data['Composite score'] = 0
    for i in range(len(base_data_array)):
        rank_factor(base_data, base_data_array[i], base_data_ascending_order_array[i])
        base_data.fillna(0)
        base_data['Composite score'] = base_data['Composite score'] + base_data['rank_perc_' + base_data_array[i]]

    comp_score_df = base_data.sort_values(by='Composite score', ascending=False)
    comp_score_df = comp_score_df.head(top_stock)

    colsz = [i + 1 for i in range(top_stock)]
    comp_score_df.index = colsz

    ticker_df = comp_score_df.loc[:, ['Ticker']]

    ticker_df.columns = [quarter]
    #ticker_df.loc[-1] = [quarter]  # adding a row
    #ticker_df.loc[0] = ['']  # adding a row

    ticker_df = ticker_df.sort_index()
    return ticker_df


def build_model():
    base_data_array = ['Dividend yield', 'Dividend growth - last 3 yrs', 'Payout ratio', 'Rev growth - fwd',
                       'Net debt/EBITDA', 'Beta']
    base_data_ascending_order_array = [False, False, True, False, True, True]

    base_data_array_temp = base_data_array.copy()
    base_data_array_temp.insert(0, 'Ticker')

    sheets_dict = pd.read_excel('/opt/airflow/dags/scripts/Raw data - style 1 (2010-2019) - Mar 06.xlsx',
                                sheet_name=None)

    final_df_arr = []

    for quarter, sheet in sheets_dict.items():

        if quarter.startswith('Q'):

            new_header = sheet.iloc[2]
            sheet = sheet[3:]
            sheet.columns = new_header

            q_date = sheet.iloc[1, 2].date()

            base_data_sheet = sheet.loc[:, base_data_array_temp]
            base_data_sheet = base_data_sheet.drop(base_data_sheet[pd.isna(base_data_sheet.Ticker)].index)

            no_of_stocks = len(base_data_sheet.index)

            result_df = calculate_composite_score(base_data_sheet,
                                                  q_date,
                                                  quarter,
                                                  base_data_array,
                                                  base_data_ascending_order_array)
            # print( result_df)
            final_df_arr.append(result_df)

    final_df = pd.concat(final_df_arr, axis=1)
    print(final_df)
    save_data_frame(final_df)



def save_data_frame(df):
    data_dict = df.to_dict("records")
    hook = MongoHook(conn_id='mongo_oracdb')
    conn: MongoClient = hook.get_conn()
    conn.get_database('oracdb').get_collection('stock_model').insert_many(data_dict)


dag = DAG(dag_id='stock_modeling', schedule_interval=None, start_date=datetime(2022, 2, 10), catchup=False)

# Operators
dummy_task = DummyOperator(task_id='dummy', dag=dag)

screening_model_op = PythonOperator(task_id='screening_model', python_callable=build_model)


def save_model(ti):
    data_dict = ti.xcom_pull(task_ids=['screening_model'])
    hook = MongoHook(conn_id='mongo_oracdb')
    conn: MongoClient = hook.get_conn()
    conn.get_database('oracdb').get_collection('stock_model').insert_one(document=data_dict)


save_model_op = PythonOperator(task_id='save_model', python_callable=save_model)

# Operator execution sequence
dummy_task.set_downstream(screening_model_op)
# screening_model_op.set_downstream(save_model_op)
